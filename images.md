# Windows

## Boot

![](http://social.technet.microsoft.com/wiki/cfs-file.ashx/__key/communityserver-wikis-components-files/00-00-00-00-05/8662.StartingWindows.jpg "")

![](http://social.technet.microsoft.com/wiki/cfs-file.ashx/__key/communityserver-wikis-components-files/00-00-00-00-05/1538.WindowsBootProcess.png "")

### Secure-Boot

![](https://technet.microsoft.com/en-us/windows/dn168167.boot_process.png "")

--------

## Windows architecture

![](https://technet.microsoft.com/dynimg/IC183863.gif "Windows NT 4.0 architecture")
![](https://technet.microsoft.com/en-us/library/cc768129.winarc01_big.gif "Simplified windows NT architecture")
![](https://msdnshared.blob.core.windows.net/media/MSDNBlogsFS/prod.evol.blogs.msdn.com/CommunityServer.Blogs.Components.WeblogFiles/00/00/00/79/60/4186.Untitled.png "")

![](https://technet.microsoft.com/dynimg/IC213680.gif "AD Windows 2000 architecture")

--------

## Windows 10 convergence

![](http://www.c-sharpcorner.com/UploadFile/1ff178/architecture-of-windows-10/Images/kal1.jpg "")

--------

## *User-mode* and *Kernel-mode*

![](https://docs.microsoft.com/en-us/windows-hardware/drivers/gettingstarted/images/userandkernelmode01.png "")

## Plug and Play

![](https://technet.microsoft.com/dynimg/IC197030.gif "")
![](https://technet.microsoft.com/dynimg/IC197031.gif "")
![](https://technet.microsoft.com/dynimg/IC196731.gif "")
